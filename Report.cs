﻿using CRUD_Operations;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class Report : Form
    {
        public Report()
        {
            InitializeComponent();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }
        //This function is used to Student management Form 
        private void StudentWindowOpen()
        {
            this.Hide();
            var newform = new Student();
            newform.Show();

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }
        //This function is used to CLO management Form 
        private void CLOFormOpen()
        {
            this.Hide();
            var newform = new CLO();
            newform.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }
        //This function is used to Rubric management Form 
        private void RubricFormOpen()
        {
            this.Hide();
            var newform = new Rubric();
            newform.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            AssessFormOpen();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            AssessFormOpen();
        }
        //This function is used to Assessment management Form 
        private void AssessFormOpen()
        {
            this.Hide();
            var newform = new Assessments();
            newform.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }

        //This function is used to Evaluation management Form 
        private void EvaluationFormOpen()
        {
            this.Hide();
            var newform = new Evaluation();
            newform.Show();

        }
        // This is the Close Label or Picture Event
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }
        //This function is used to Attendence management Form 
        private void AttendenceFormOpen()
        {
            this.Hide();
            var newform = new Attendence();
            newform.Show();
        }

        private void ActiveStudents_Button_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Concat(FirstName,' ',LastName) As Name,RegistrationNumber,'Active' AS Status from Student where Status = 5 ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void InavtiveStudent_button_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Concat(FirstName,' ',LastName) As Name,RegistrationNumber,'Inactive' AS Status from Student where Status = 6", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)
            {
                SaveFileDialog save = new SaveFileDialog();
                save.Filter = "PDF (*.pdf)|*.pdf";
                save.FileName = "Result.pdf";
                bool ErrorMessage = false;
                if (save.ShowDialog() == DialogResult.OK)
                {
                    if (File.Exists(save.FileName))
                    {
                        try
                        {
                            File.Delete(save.FileName);
                        }
                        catch (Exception ex)
                        {
                            ErrorMessage = true;
                            MessageBox.Show("Unable to wride data in disk" + ex.Message);
                        }
                    }
                    if (!ErrorMessage)
                    {
                        try
                        {
                            PdfPTable pTable = new PdfPTable(dataGridView1.Columns.Count);
                            pTable.DefaultCell.Padding = 2;
                            pTable.WidthPercentage = 100;
                            pTable.HorizontalAlignment = Element.ALIGN_LEFT;
                            foreach (DataGridViewColumn col in dataGridView1.Columns)
                            {
                                PdfPCell pCell = new PdfPCell(new Phrase(col.HeaderText));
                                pTable.AddCell(pCell);
                            }
                            foreach (DataGridViewRow viewRow in dataGridView1.Rows)
                            {
                                foreach (DataGridViewCell dcell in viewRow.Cells)
                                {
                                    if (dcell.Value != null)  // Check if Value is not null before accessing it
                                    {
                                        pTable.AddCell(dcell.Value.ToString());
                                    }
                                    else
                                    {
                                        // Handle the case where Value is null (if needed)
                                        pTable.AddCell(""); // Add an empty cell or handle it according to your requirements
                                    }
                                }
                            }
                            using (FileStream fileStream = new FileStream(save.FileName, FileMode.Create))
                            {
                                Document document = new Document(PageSize.A4, 8f, 16f, 16f, 8f);
                                PdfWriter.GetInstance(document, fileStream);
                                document.Open();
                                document.Add(pTable);
                                document.Close();
                                fileStream.Close();
                            }
                            MessageBox.Show("Data Export Successfully", "info");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Error while exporting Data" + ex.Message);
                        }
                    }
                }
            }
            else
            {
                MessageBox.Show("No Record Found!", "Info");
            }
        }

        private void CloResult_button_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Concat(Student . FirstName,' ', Student . LastName) As StudentName , Student.RegistrationNumber,Clo.Name ,SUM( AssessmentComponent . TotalMarks ) As TotalMarks,SUM( (CAST( RubricLevel . MeasurementLevel As FLOAT) / [ max measurement level ] . maxLevel ) * AssessmentComponent . TotalMarks ) [ Obtained Marks ] ,( ( (SUM( (CAST( RubricLevel . MeasurementLevel As FLOAT) /[ max measurement level ] . maxLevel ) * AssessmentComponent . TotalMarks ) ) * 100 ) /(SUM( AssessmentComponent.TotalMarks ) ) ) [ Percentage ] FROM Student INNER JOIN StudentResult ON Student.Id = StudentResult . StudentId INNER JOIN RubricLevel ON StudentResult . RubricMeasurementId = RubricLevel . Id INNER JOIN AssessmentComponent ON AssessmentComponent . Id = StudentResult . AssessmentComponentId INNER JOIN Assessment ON Assessment . Id = AssessmentComponent .AssessmentId INNER JOIN Rubric ON Rubric . Id = RubricLevel . RubricId INNER JOIN Clo ON Clo . Id = Rubric . CloId INNER JOIN (SELECT RubricLevel . RubricId Rid , MAX( RubricLevel . MeasurementLevel ) maxLevel FROM RubricLevel GROUP BY RubricLevel . RubricId ) [ max measurement level ] ON [ max measurement level ] . Rid = Rubric . Id GROUP BY Clo . Name, Student . FirstName , Student . LastName , Student . RegistrationNumber", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Assessment_button_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Concat(Std. FirstName,' ',Std. LastName ) As StudentName, Std. RegistrationNumber As RegistrationNumber  ,Assessment . Title As AssessmentTitle  , SUM( AssessmentComponent . TotalMarks ) As TotalMarks  ,SUM( (CAST( RubricLevel . MeasurementLevel AS FLOAT) /[ MaximumMeasurementLevel ] . maximum) * AssessmentComponent . TotalMarks ) As ObtainedMarks  ,( (SUM( (CAST( RubricLevel . MeasurementLevel AS FLOAT) /[ MaximumMeasurementLevel ] . maximum) * AssessmentComponent . TotalMarks ) ) /SUM( AssessmentComponent . TotalMarks ) ) * 100 As [Percentage] FROM Student AS Std INNER JOIN StudentResult ON Std.Id = StudentResult. StudentId INNER JOIN RubricLevel ON StudentResult . RubricMeasurementId = RubricLevel.Id INNER JOIN AssessmentComponent ON AssessmentComponent . Id = StudentResult.AssessmentComponentId INNER JOIN Assessment ON Assessment.Id = AssessmentComponent . AssessmentId INNER JOIN Rubric ON Rubric . Id = RubricLevel . RubricId INNER JOIN (SELECT RubricId , MAX( MeasurementLevel ) AS maximum FROM RubricLevel GROUP BY RubricId ) AS [ MaximumMeasurementLevel ] ON [ MaximumMeasurementLevel ] . RubricId = Rubric.Id GROUP BY Std . FirstName ,Std . LastName ,Std. RegistrationNumber ,Assessment . Title", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void Attendence_button_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("SELECT Concat(Student . FirstName,' ', Student . LastName ) As StudentName , Student . RegistrationNumber As RegistrationNumber  , Lookup . Name AS AttendanceStatus , ClassAttendance . AttendanceDate FROM Student INNER JOIN StudentAttendance ON Student . Id = StudentAttendance . StudentId INNER JOIN ClassAttendance ON ClassAttendance . Id = StudentAttendance . AttendanceId INNER JOIN Lookup ON Lookup . LookupId = StudentAttendance . AttendanceStatus ORDER BY AttendanceDate\r\n", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }
    }
}
