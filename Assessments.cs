﻿using CRUD_Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace DBMidProject
{
    public partial class Assessments : Form
    {
        public Assessments()
        {
            InitializeComponent();
            loadDatainGrid();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }
        //This function is used to Student management Form 
        private void StudentWindowOpen()
        {
            this.Hide();
            var newform = new Student();
            newform.Show();

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }
        //This function is used to CLO management Form 
        private void CLOFormOpen()
        {
            this.Hide();
            var newform = new CLO();
            newform.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }
        //This function is used to Rubric management Form 
        private void RubricFormOpen()
        {
            this.Hide();
            var newform = new Rubric();
            newform.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }
        //This function is used to Evaluation management Form 
        private void EvaluationFormOpen()
        {
            this.Hide();
            var newform = new Evaluation();
            newform.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }
        //This function is used to Report management Form 
        private void ReportFormOpen()
        {
            this.Hide();
            var newform = new Report();
            newform.Show();
        }
        // This is the Close Label or Picture Event
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }
        //This function is used to Attendence management Form 
        private void AttendenceFormOpen()
        {
            this.Hide();
            var newform = new Attendence();
            newform.Show();
        }

        private void loadDatainGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }
        public bool isvalid()
        {
            if (title.Text == string.Empty || dateTimePicker1.Value.ToString() == string.Empty || totalmarks.Text == string.Empty || weightage.Text == string.Empty)
                return false;
            return true;
        }
        public void ClearTextBox()
        {
            title.Clear();
            dateTimePicker1.Refresh();
            totalmarks.Clear();
            weightage.Clear();
        }
        private void ADD_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {


                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title, @DateCreated, @TotalMarks,@TotalWeightage)", con);
                cmd.Parameters.AddWithValue("@Title", title.Text);
                cmd.Parameters.AddWithValue("@DateCreated", dateTimePicker1.Value.ToString());
                cmd.Parameters.AddWithValue("@TotalMarks", totalmarks.Text);
                cmd.Parameters.AddWithValue("@TotalWeightage", weightage.Text);


                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Added New Assessment");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("All Field Required");
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                title.Text = selectedRow.Cells["Title"].Value.ToString();
                dateTimePicker1.Text = selectedRow.Cells["DateCreated"].Value.ToString();
                totalmarks.Text = selectedRow.Cells["TotalMarks"].Value.ToString();
                weightage.Text = selectedRow.Cells["TotalWeightage"].Value.ToString();
                assessmentid.Text = selectedRow.Cells["Id"].Value.ToString();
            }
        }

        private void Del_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete From Assessment where Title=@Title ", con);
                cmd.Parameters.AddWithValue("@Title", title.Text);



                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Deleted New Assessment");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Any Assessment that you want to deleted");
            }
        }

        private void update_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Assessment set Title=@Title,DateCreated=@DateCreated,TotalMarks=@TotalMarks,TotalWeightage=@TotalWeightage where Id=@Id  ", con);
                cmd.Parameters.AddWithValue("@Title", title.Text);
                cmd.Parameters.AddWithValue("@DateCreated", dateTimePicker1.Value.ToString());
                cmd.Parameters.AddWithValue("@TotalMarks", totalmarks.Text);
                cmd.Parameters.AddWithValue("@TotalWeightage", weightage.Text);
                cmd.Parameters.AddWithValue("@Id", assessmentid.Text);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Updated");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Any Rubric that you want to Updated");
            }
        }

        private void Rubric_Level_Button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var newform = new AssessmentComponents();
            newform.Show();
        }
    }
}
