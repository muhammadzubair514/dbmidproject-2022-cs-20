﻿using CRUD_Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace DBMidProject
{
    public partial class AssessmentComponents : Form
    {
        public AssessmentComponents()
        {
            InitializeComponent();
            loaddataintable();
            loadRubric();
            loadAssessementId();
        }

        private void Exit_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var newform = new Assessments();
            newform.Show();
        }
        public void loadRubric()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric", con);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    RidList.Items.Add(reader.GetInt32(0));
                }
            }

            reader.Close();
            cmd.ExecuteNonQuery();
        }
        public void loadAssessementId()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Assessment", con);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    assessid.Items.Add(reader.GetInt32(0));
                }
            }

            reader.Close();
            cmd.ExecuteNonQuery();
        }
        private void loaddataintable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void ADD_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name, @RubricId,@TotalMarks,@DateCreated,@DateUpdated,@AssessmentId)", con);
                cmd.Parameters.AddWithValue("@Name", rname.Text);
                cmd.Parameters.AddWithValue("@RubricId", int.Parse(RidList.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(assessid.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@TotalMarks", totalmarks.Text);
                cmd.Parameters.AddWithValue("@DateCreated", dateTimePicker1.Value.ToString());
                cmd.Parameters.AddWithValue("@DateUpdated", dateTimePicker1.Value.ToString());


                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Added New Assessment Component");
                loaddataintable();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("All fields Required");
            }
        }
        public bool isvalid()
        {
            if (rname.Text == string.Empty || dateTimePicker1.Value.ToString() == string.Empty || totalmarks.Text == string.Empty || assessid.Text == string.Empty || dateTimePicker2.Value.ToString() == string.Empty|| RidList.Text == string.Empty)
                return false;
            return true;
        }
        public void ClearTextBox()
        {
            rname.Clear();
            dateTimePicker1.Refresh();
            dateTimePicker2.Refresh();
            totalmarks.Clear();
            assessid.SelectedItem = null;
            RidList.SelectedItem = null;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                rname.Text = selectedRow.Cells["Name"].Value.ToString();
                dateTimePicker1.Text = selectedRow.Cells["DateCreated"].Value.ToString();
                dateTimePicker2.Text = selectedRow.Cells["DateUpdated"].Value.ToString();
                totalmarks.Text = selectedRow.Cells["TotalMarks"].Value.ToString();
                RidList.Text = selectedRow.Cells["RubricId"].Value.ToString();
                assessid.Text = selectedRow.Cells["AssessmentId"].Value.ToString();
                asid.Text = selectedRow.Cells["Id"].Value.ToString();
            }
        }

        private void Del_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete From AssessmentComponent Where Id=@Id", con);
                cmd.Parameters.AddWithValue("@Id", asid.Text);
                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Deleted Assessment Component");
                loaddataintable();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Assessment Component that you want to Delete");
            }

        }

        private void update_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update AssessmentComponent set Name = @Name,RubricID=@RubricID,AssessmentId=@AssessmentID,TotalMarks=@TotalMarks,DateCreated=@DateCreated,DateUpdated=@DateUpdated where Id=@Id  ", con);
                cmd.Parameters.AddWithValue("@Id", asid.Text);
                cmd.Parameters.AddWithValue("@Name", rname.Text);
                cmd.Parameters.AddWithValue("@RubricId", int.Parse(RidList.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@AssessmentId", int.Parse(assessid.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@TotalMarks", totalmarks.Text);
                cmd.Parameters.AddWithValue("@DateCreated", dateTimePicker1.Value.ToString());
                cmd.Parameters.AddWithValue("@DateUpdated", dateTimePicker1.Value.ToString());
                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Updated");
                loaddataintable();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Any Rubric that you want to Updated");
            }
        }
    }
}
