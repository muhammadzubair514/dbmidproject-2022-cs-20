﻿using CRUD_Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DBMidProject
{
    public partial class RubricLevel : Form
    {
        public RubricLevel()
        {
            InitializeComponent();
            loaddataintable();
            loadcomboboxvalues();

        }

        private void Exit_button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var newform = new Rubric();
            newform.Show();
        }
        private void loaddataintable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        public void loadcomboboxvalues()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric", con);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    RidList.Items.Add(reader.GetInt32(0));
                }
            }

            reader.Close();
            cmd.ExecuteNonQuery();
        }

        private void ADD_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd = new SqlCommand("if not exists (select top 1 RubricId from RubricLevel where RubricId=@RubricId) begin INSERT INTO RubricLevel (Details, RubricId, MeasurementLevel) SELECT DISTINCT Details, @RubricId AS RubricId, MeasurementLevel FROM RubricLevel order by MeasurementLevel end", con);
                cmd.Parameters.AddWithValue("@RubricId", int.Parse(RidList.SelectedItem.ToString()));


                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Added New Rubric Level");
                loaddataintable();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("All fields Required");
            }

        }
        public bool isvalid()
        {
            if (RidList.Text == string.Empty )
            {
                return false;
            }
            return true;
        }
        public void ClearTextBox()
        {
           
            RidList.SelectedItem = null;

        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex];
                RidList.Text = selectedRow.Cells["RubricId"].Value.ToString();
                rlid.Text = selectedRow.Cells["ID"].Value.ToString();
            }

        }



    }
}
