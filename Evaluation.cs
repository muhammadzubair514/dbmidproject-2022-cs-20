﻿using CRUD_Operations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace DBMidProject
{
    public partial class Evaluation : Form
    {
        public Evaluation()
        {
            InitializeComponent();
            loadDatainGrid();
            loadstudentid();
            loadassessmentid();
            loadRubricMeasurementid();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }
        //This function is used to Student management Form 
        private void StudentWindowOpen()
        {
            this.Hide();
            var newform = new Student();
            newform.Show();

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }
        //This function is used to CLO management Form 
        private void CLOFormOpen()
        {
            this.Hide();
            var newform = new CLO();
            newform.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }
        //This function is used to Rubric management Form 
        private void RubricFormOpen()
        {
            this.Hide();
            var newform = new Rubric();
            newform.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            AssessFormOpen();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            AssessFormOpen();
        }
        //This function is used to Assessment management Form 
        private void AssessFormOpen()
        {
            this.Hide();
            var newform = new Assessments();
            newform.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }
        //This function is used to Report management Form 
        private void ReportFormOpen()
        {
            this.Hide();
            var newform = new Report();
            newform.Show();
        }
        // This is the Close Label or Picture Event
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        public void loadstudentid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Student Where Status = 5", con);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    SidList.Items.Add(reader.GetInt32(0));
                }
            }

            reader.Close();
            cmd.ExecuteNonQuery();
        }
        public void loadassessmentid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from AssessmentComponent", con);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    ACid.Items.Add(reader.GetInt32(0));
                }
            }

            reader.Close();
            cmd.ExecuteNonQuery();
        }
        public void loadRubricMeasurementid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id  from RubricLevel", con);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    rmid.Items.Add(reader.GetInt32(0));
                }
            }

            reader.Close();
            cmd.ExecuteNonQuery();
        }
        private void loadDatainGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from StudentResult  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        private void ADD_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd = new SqlCommand("Insert into StudentResult values (@StudentId,@AssessmentComponentId,@RubricMeasurementId,@EvaluationDate)", con);
                cmd.Parameters.AddWithValue("@StudentId", int.Parse(SidList.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@AssessmentComponentId", int.Parse(ACid.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@RubricMeasurementId", int.Parse(rmid.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Value.ToString());


                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Added New Evaluation");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("All fields Required");
            }
        }

        public bool isvalid()
        {
            if (SidList.Text == string.Empty || dateTimePicker1.Value.ToString() == string.Empty || ACid.Text==String.Empty|| rmid.Text==string.Empty)
                return false;
            return true;
        }
        public void ClearTextBox()
        {
            dateTimePicker1.Refresh();
            SidList.SelectedItem = null;
            ACid.SelectedItem = null;
            rmid.SelectedItem = null;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                SidList.Text = selectedRow.Cells["StudentId"].Value.ToString();
                dateTimePicker1.Text = selectedRow.Cells["EvaluationDate"].Value.ToString();
                ACid.Text = selectedRow.Cells["AssessmentComponentId"].Value.ToString();
                rmid.Text = selectedRow.Cells["RubricMeasurementId"].Value.ToString();

            }
        }

        private void Del_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd = new SqlCommand("Delete From StudentResult Where StudentId=@StudentId", con);
                cmd.Parameters.AddWithValue("@StudentId", int.Parse(SidList.SelectedItem.ToString()));


                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Deleted Evatuation");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select the Evaluation tha you want to delete");
            }
        }

        private void update_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update StudentResult set AssessmentComponentId=@AssessmentComponentId, RubricMeasurementId=@RubricMeasurementId,StudentId=@StudentId where EvaluationDate=@EvaluationDate  ", con);
                cmd.Parameters.AddWithValue("@StudentId", int.Parse(SidList.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@AssessmentComponentId", int.Parse(ACid.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@RubricMeasurementId", int.Parse(rmid.SelectedItem.ToString()));
                cmd.Parameters.AddWithValue("@EvaluationDate", dateTimePicker1.Value.ToString());

                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Updated");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Any Evaluation that you want to Updated");
            }
        }
    }
}
