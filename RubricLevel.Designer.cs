﻿namespace DBMidProject
{
    partial class RubricLevel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label2 = new System.Windows.Forms.Label();
            this.RidList = new System.Windows.Forms.ComboBox();
            this.Rid = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Exit_button = new System.Windows.Forms.Button();
            this.rlid = new System.Windows.Forms.Label();
            this.cloID = new System.Windows.Forms.Label();
            this.ADD = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(288, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(461, 41);
            this.label2.TabIndex = 9;
            this.label2.Text = "Rubrics Level Management";
            // 
            // RidList
            // 
            this.RidList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RidList.FormattingEnabled = true;
            this.RidList.Location = new System.Drawing.Point(118, 96);
            this.RidList.Name = "RidList";
            this.RidList.Size = new System.Drawing.Size(157, 23);
            this.RidList.TabIndex = 52;
            // 
            // Rid
            // 
            this.Rid.AutoSize = true;
            this.Rid.Cursor = System.Windows.Forms.Cursors.Default;
            this.Rid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rid.ForeColor = System.Drawing.Color.Firebrick;
            this.Rid.Location = new System.Drawing.Point(27, 99);
            this.Rid.Name = "Rid";
            this.Rid.Size = new System.Drawing.Size(85, 20);
            this.Rid.TabIndex = 46;
            this.Rid.Text = "Rubric ID";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(31, 229);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(924, 260);
            this.dataGridView1.TabIndex = 55;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // Exit_button
            // 
            this.Exit_button.BackColor = System.Drawing.Color.MediumPurple;
            this.Exit_button.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit_button.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Exit_button.Location = new System.Drawing.Point(551, 152);
            this.Exit_button.Name = "Exit_button";
            this.Exit_button.Size = new System.Drawing.Size(139, 33);
            this.Exit_button.TabIndex = 56;
            this.Exit_button.Text = "Exit";
            this.Exit_button.UseVisualStyleBackColor = false;
            this.Exit_button.Click += new System.EventHandler(this.Exit_button_Click);
            // 
            // rlid
            // 
            this.rlid.AutoSize = true;
            this.rlid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rlid.Location = new System.Drawing.Point(232, 206);
            this.rlid.Name = "rlid";
            this.rlid.Size = new System.Drawing.Size(37, 20);
            this.rlid.TabIndex = 58;
            this.rlid.Text = "- - -";
            // 
            // cloID
            // 
            this.cloID.AutoSize = true;
            this.cloID.Cursor = System.Windows.Forms.Cursors.Default;
            this.cloID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cloID.ForeColor = System.Drawing.SystemColors.Highlight;
            this.cloID.Location = new System.Drawing.Point(30, 202);
            this.cloID.Name = "cloID";
            this.cloID.Size = new System.Drawing.Size(207, 20);
            this.cloID.TabIndex = 57;
            this.cloID.Text = "Selected Rubric Level is ";
            // 
            // ADD
            // 
            this.ADD.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ADD.Location = new System.Drawing.Point(295, 152);
            this.ADD.Name = "ADD";
            this.ADD.Size = new System.Drawing.Size(131, 33);
            this.ADD.TabIndex = 48;
            this.ADD.Text = "ADD";
            this.ADD.UseVisualStyleBackColor = false;
            this.ADD.Click += new System.EventHandler(this.ADD_Click);
            // 
            // RubricLevel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(998, 506);
            this.Controls.Add(this.rlid);
            this.Controls.Add(this.cloID);
            this.Controls.Add(this.Exit_button);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.RidList);
            this.Controls.Add(this.ADD);
            this.Controls.Add(this.Rid);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RubricLevel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "RubricLevel";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox RidList;
        private System.Windows.Forms.Label Rid;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button Exit_button;
        private System.Windows.Forms.Label rlid;
        private System.Windows.Forms.Label cloID;
        private System.Windows.Forms.Button ADD;
    }
}