﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using static System.Collections.Specialized.BitVector32;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBMidProject
{

    public partial class Student : Form
    {
        public int a = 5;
        public Student()
        {
            InitializeComponent();
            loadDataINGrid();
            comboBox1.SelectedIndex = 0;
           



        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }
        //This function is used to CLO management Form 
        private void CLOFormOpen()
        {
            this.Hide();
            var newform = new CLO();
            newform.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            RubricsFormOpen();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            RubricsFormOpen();
        }
        //This function is used to Rubrics management Form 
        private void RubricsFormOpen()
        {
            this.Hide();
            var newform = new Rubric();
            newform.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            AssessmentFormOpen();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            AssessmentFormOpen();
        }
        //This function is used to Assessment management Form 
        private void AssessmentFormOpen()
        {
            this.Hide();
            var newform = new Assessments();
            newform.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }
        //This function is used to Evaluation management Form 
        private void EvaluationFormOpen()
        {
            this.Hide();
            var newform = new Evaluation();
            newform.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }
        //This function is used to Report management Form 
        private void ReportFormOpen()
        {
            this.Hide();
            var newform = new Report();
            newform.Show();
        }
        // This is the Close Label or Picture Event
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void update_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd = new SqlCommand("Update Student set FirstName = @FirstName, LastName = @LastName, Contact = @Contact, Email = @Email, RegistrationNumber = @RegistrationNumber, Status = @Status WHERE RegistrationNumber = @RegistrationNumber ", con);

                cmd.Parameters.AddWithValue("@LastName", SLName.Text);
                cmd.Parameters.AddWithValue("@FirstName", SFName.Text);
                cmd.Parameters.AddWithValue("@Contact", Scontact.Text);
                cmd.Parameters.AddWithValue("@Email", SEmail.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", SRegNO.Text);
                cmd.Parameters.AddWithValue("@Status", a);


                cmd.ExecuteNonQuery();

                MessageBox.Show("Successfully Updated");
                loadDataINGrid();
                ClearTextBox();

            }
            else
                MessageBox.Show("Select any Row that you want to Update");
        }
        private void ADD_Click(object sender, EventArgs e)
        {
            if (isValid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName, @LastName, @Contact ,@Email,@RegistrationNumber,@Status)", con);
                cmd.Parameters.AddWithValue("@LastName", SLName.Text);
                cmd.Parameters.AddWithValue("@FirstName", SFName.Text);
                cmd.Parameters.AddWithValue("@Contact", Scontact.Text);
                cmd.Parameters.AddWithValue("@Email", SEmail.Text);
                cmd.Parameters.AddWithValue("@RegistrationNumber", SRegNO.Text);

                cmd.Parameters.AddWithValue("@Status", a);

                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully saved");
                loadDataINGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("All field Required");
            }


        }

        //This Function Load DataFrom Excel File To MSSQL
        /* public void DataLoad()
         {
             var con = Configuration.getInstance().getConnection();
             SqlCommand cmd = new SqlCommand("Insert into Student (FirstName, LastName, Contact, Email, RegistrationNumber, [Status]) values (@FirstName, @LastName, @Contact, @Email, @RegistrationNumber, @Status)", con);


             string[] csvLines = File.ReadAllLines("Student_Data.csv.csv");

             foreach (var line in csvLines.Skip(0))
             {
                 string[] values = line.Split(',');
                 cmd.Parameters.Clear();
                 cmd.Parameters.AddWithValue("@FirstName", values[3]);
                 cmd.Parameters.AddWithValue("@LastName", values[4]);
                 cmd.Parameters.AddWithValue("@Contact", values[5]);
                 cmd.Parameters.AddWithValue("@Email", values[6]);
                 cmd.Parameters.AddWithValue("@RegistrationNumber", values[2]);
                 cmd.Parameters.AddWithValue("@Status", 1);

                 cmd.ExecuteNonQuery();
             }

         } */
        //This function load data in Data Grid View

        public void loadDataINGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select FirstName,LastName,Contact,Email,RegistrationNumber from Student where Status = 5 ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {

            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                SLName.Text = selectedRow.Cells["LastName"].Value.ToString();
                SFName.Text = selectedRow.Cells["FirstName"].Value.ToString();
                Scontact.Text = selectedRow.Cells["Contact"].Value.ToString();
                SEmail.Text = selectedRow.Cells["Email"].Value.ToString();
                SRegNO.Text = selectedRow.Cells["RegistrationNumber"].Value.ToString();


            }
        }

        public bool isValid()
        {
            if (SRegNO.Text == string.Empty || SFName.Text == string.Empty || SLName.Text == string.Empty || Scontact.Text == string.Empty || SEmail.Text == string.Empty || comboBox1.Text == string.Empty)
            {
                return false;
            }
            return true;
        }

        public void ClearTextBox()
        {
            SRegNO.Clear();
            SFName.Clear();
            SLName.Clear();
            Scontact.Clear();
            SEmail.Clear();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem.ToString() == "Active")
            {

                a = 5;
            }

        }




        //This but deleted the student from UI but not For databae
        private void Del_Click(object sender, EventArgs e)
        {
            if (SRegNO.Text != string.Empty)
            {
                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd = new SqlCommand("Update Student set  Status = @Status WHERE RegistrationNumber = @RegistrationNumber ", con);

                cmd.Parameters.AddWithValue("@RegistrationNumber", SRegNO.Text);
                a = 6;
                cmd.Parameters.AddWithValue("@Status", a);


                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully Deleted");
                loadDataINGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Please Select the Student that you want to deleted");
            }
        }

        private void Search_button_Click(object sender, EventArgs e)
        {
            LButton.Visible = true;
            if (SRegNO.Text != string.Empty)
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Select FirstName,LastName,Contact,Email,RegistrationNumber from Student where RegistrationNumber = @RegistrationNumber And Status=5", con);
                cmd.Parameters.AddWithValue("@RegistrationNumber", SRegNO.Text);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                da.Fill(dt);
                dataGridView1.DataSource = dt;
                cmd.ExecuteNonQuery();

                SRegNO.Clear();

            }
            else
            {
                MessageBox.Show("Enter Registration Number");
               
            }


        }

        private void LButton_Click(object sender, EventArgs e)
        {
            loadDataINGrid();
            LButton.Visible = false;
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }
        //This function is used to Attendence management Form 
        private void AttendenceFormOpen()
        {
            this.Hide();
            var newform = new Attendence();
            newform.Show();
        }


    }
    }
