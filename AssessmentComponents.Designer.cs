﻿namespace DBMidProject
{
    partial class AssessmentComponents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.asid = new System.Windows.Forms.Label();
            this.cloID = new System.Windows.Forms.Label();
            this.Exit_button = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.assessid = new System.Windows.Forms.ComboBox();
            this.ML = new System.Windows.Forms.Label();
            this.RidList = new System.Windows.Forms.ComboBox();
            this.name = new System.Windows.Forms.Label();
            this.update = new System.Windows.Forms.Button();
            this.ADD = new System.Windows.Forms.Button();
            this.rname = new System.Windows.Forms.TextBox();
            this.Rid = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.totalmarks = new System.Windows.Forms.TextBox();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label11 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.ID = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // asid
            // 
            this.asid.AutoSize = true;
            this.asid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.asid.Location = new System.Drawing.Point(347, 273);
            this.asid.Name = "asid";
            this.asid.Size = new System.Drawing.Size(37, 20);
            this.asid.TabIndex = 72;
            this.asid.Text = "- - -";
            // 
            // cloID
            // 
            this.cloID.AutoSize = true;
            this.cloID.Cursor = System.Windows.Forms.Cursors.Default;
            this.cloID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cloID.ForeColor = System.Drawing.SystemColors.Highlight;
            this.cloID.Location = new System.Drawing.Point(43, 271);
            this.cloID.Name = "cloID";
            this.cloID.Size = new System.Drawing.Size(303, 20);
            this.cloID.TabIndex = 71;
            this.cloID.Text = "Selected Assessment Component is ";
            // 
            // Exit_button
            // 
            this.Exit_button.BackColor = System.Drawing.Color.MediumPurple;
            this.Exit_button.Font = new System.Drawing.Font("Arial Black", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Exit_button.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Exit_button.Location = new System.Drawing.Point(822, 219);
            this.Exit_button.Name = "Exit_button";
            this.Exit_button.Size = new System.Drawing.Size(139, 33);
            this.Exit_button.TabIndex = 70;
            this.Exit_button.Text = "Exit";
            this.Exit_button.UseVisualStyleBackColor = false;
            this.Exit_button.Click += new System.EventHandler(this.Exit_button_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(39, 298);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView1.Size = new System.Drawing.Size(924, 260);
            this.dataGridView1.TabIndex = 69;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // assessid
            // 
            this.assessid.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.assessid.FormattingEnabled = true;
            this.assessid.Location = new System.Drawing.Point(801, 96);
            this.assessid.Name = "assessid";
            this.assessid.Size = new System.Drawing.Size(157, 23);
            this.assessid.TabIndex = 68;
            // 
            // ML
            // 
            this.ML.AutoSize = true;
            this.ML.Cursor = System.Windows.Forms.Cursors.Default;
            this.ML.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ML.ForeColor = System.Drawing.Color.Firebrick;
            this.ML.Location = new System.Drawing.Point(654, 97);
            this.ML.Name = "ML";
            this.ML.Size = new System.Drawing.Size(131, 20);
            this.ML.TabIndex = 67;
            this.ML.Text = "Assessment ID";
            // 
            // RidList
            // 
            this.RidList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RidList.FormattingEnabled = true;
            this.RidList.Location = new System.Drawing.Point(143, 96);
            this.RidList.Name = "RidList";
            this.RidList.Size = new System.Drawing.Size(186, 23);
            this.RidList.TabIndex = 66;
            // 
            // name
            // 
            this.name.AutoSize = true;
            this.name.Cursor = System.Windows.Forms.Cursors.Default;
            this.name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.name.ForeColor = System.Drawing.Color.Firebrick;
            this.name.Location = new System.Drawing.Point(345, 97);
            this.name.Name = "name";
            this.name.Size = new System.Drawing.Size(55, 20);
            this.name.TabIndex = 65;
            this.name.Text = "Name";
            // 
            // update
            // 
            this.update.BackColor = System.Drawing.Color.MediumAquamarine;
            this.update.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.update.Location = new System.Drawing.Point(511, 221);
            this.update.Name = "update";
            this.update.Size = new System.Drawing.Size(110, 27);
            this.update.TabIndex = 64;
            this.update.Text = "Update";
            this.update.UseVisualStyleBackColor = false;
            this.update.Click += new System.EventHandler(this.update_Click);
            // 
            // ADD
            // 
            this.ADD.BackColor = System.Drawing.Color.MediumAquamarine;
            this.ADD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ADD.Location = new System.Drawing.Point(331, 221);
            this.ADD.Name = "ADD";
            this.ADD.Size = new System.Drawing.Size(108, 27);
            this.ADD.TabIndex = 62;
            this.ADD.Text = "ADD";
            this.ADD.UseVisualStyleBackColor = false;
            this.ADD.Click += new System.EventHandler(this.ADD_Click);
            // 
            // rname
            // 
            this.rname.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.rname.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rname.Location = new System.Drawing.Point(468, 95);
            this.rname.Name = "rname";
            this.rname.Size = new System.Drawing.Size(153, 22);
            this.rname.TabIndex = 61;
            // 
            // Rid
            // 
            this.Rid.AutoSize = true;
            this.Rid.Cursor = System.Windows.Forms.Cursors.Default;
            this.Rid.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Rid.ForeColor = System.Drawing.Color.Firebrick;
            this.Rid.Location = new System.Drawing.Point(35, 100);
            this.Rid.Name = "Rid";
            this.Rid.Size = new System.Drawing.Size(85, 20);
            this.Rid.TabIndex = 60;
            this.Rid.Text = "Rubric ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(241, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(637, 41);
            this.label2.TabIndex = 59;
            this.label2.Text = "Assessment Component Management";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Cursor = System.Windows.Forms.Cursors.Default;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Firebrick;
            this.label1.Location = new System.Drawing.Point(35, 151);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 20);
            this.label1.TabIndex = 74;
            this.label1.Text = "Total Marks";
            // 
            // totalmarks
            // 
            this.totalmarks.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.totalmarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.totalmarks.Location = new System.Drawing.Point(143, 149);
            this.totalmarks.Name = "totalmarks";
            this.totalmarks.Size = new System.Drawing.Size(186, 22);
            this.totalmarks.TabIndex = 73;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker2.Location = new System.Drawing.Point(801, 150);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(157, 21);
            this.dateTimePicker2.TabIndex = 78;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Cursor = System.Windows.Forms.Cursors.Default;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Firebrick;
            this.label11.Location = new System.Drawing.Point(654, 149);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(122, 20);
            this.label11.TabIndex = 77;
            this.label11.Text = "Date Updated";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker1.Location = new System.Drawing.Point(468, 151);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(153, 21);
            this.dateTimePicker1.TabIndex = 76;
            // 
            // ID
            // 
            this.ID.AutoSize = true;
            this.ID.Cursor = System.Windows.Forms.Cursors.Default;
            this.ID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ID.ForeColor = System.Drawing.Color.Firebrick;
            this.ID.Location = new System.Drawing.Point(345, 151);
            this.ID.Name = "ID";
            this.ID.Size = new System.Drawing.Size(117, 20);
            this.ID.TabIndex = 75;
            this.ID.Text = "Date Created";
            // 
            // AssessmentComponents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Window;
            this.ClientSize = new System.Drawing.Size(998, 611);
            this.Controls.Add(this.dateTimePicker2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dateTimePicker1);
            this.Controls.Add(this.ID);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.totalmarks);
            this.Controls.Add(this.asid);
            this.Controls.Add(this.cloID);
            this.Controls.Add(this.Exit_button);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.assessid);
            this.Controls.Add(this.ML);
            this.Controls.Add(this.RidList);
            this.Controls.Add(this.name);
            this.Controls.Add(this.update);
            this.Controls.Add(this.ADD);
            this.Controls.Add(this.rname);
            this.Controls.Add(this.Rid);
            this.Controls.Add(this.label2);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AssessmentComponents";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AssessmentComponents";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label asid;
        private System.Windows.Forms.Label cloID;
        private System.Windows.Forms.Button Exit_button;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox assessid;
        private System.Windows.Forms.Label ML;
        private System.Windows.Forms.ComboBox RidList;
        private System.Windows.Forms.Label name;
        private System.Windows.Forms.Button update;
        private System.Windows.Forms.Button ADD;
        private System.Windows.Forms.TextBox rname;
        private System.Windows.Forms.Label Rid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox totalmarks;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label ID;
    }
}