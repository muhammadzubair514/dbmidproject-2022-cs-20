﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace DBMidProject
{
    public partial class Rubric : Form
    {
        public Rubric()
        {
            InitializeComponent();
            loaddataintable();
            loadcomboboxvalues();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }

        //This function is used to Student management Form 
        private void StudentWindowOpen()
        {
            this.Hide();
            var newform = new Student();
            newform.Show();

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }
        //This function is used to CLO management Form 
        private void CLOFormOpen()
        {
            this.Hide();
            var newform = new CLO();
            newform.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            AssessmentFormOpen();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            AssessmentFormOpen();
        }
        //This function is used to Assessment management Form 
        private void AssessmentFormOpen()
        {
            this.Hide();
            var newform = new Assessments();
            newform.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }
        //This function is used to Evaluation management Form 
        private void EvaluationFormOpen()
        {
            this.Hide();
            var newform = new Evaluation();
            newform.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }
        //This function is used to Report management Form 
        private void ReportFormOpen()
        {
            this.Hide();
            var newform = new Report();
            newform.Show();
        }

        // This is the Close Label or Picture Event
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }
        //This function is used to Attendence management Form 
        private void AttendenceFormOpen()
        {
            this.Hide();
            var newform = new Attendence();
            newform.Show();
        }

        private void loaddataintable()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        public void loadcomboboxvalues()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Clo", con);

            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    CidList.Items.Add(reader.GetInt32(0));
                }
            }

            reader.Close();
            cmd.ExecuteNonQuery();
        }
        public bool isvalid()
        {
            if (CidList.Text == string.Empty || detail.Text == string.Empty)
            {
                return false;
            }
            return true;
        }

        private void ADD_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Rubric values ((SELECT ISNULL(MAX(Id) + 1, 1)  FROM Rubric),@Details, @CloId)", con);
                cmd.Parameters.AddWithValue("@Details", detail.Text);
                cmd.Parameters.AddWithValue("@CloId", int.Parse(CidList.SelectedItem.ToString()));


                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Added New Rubric");
                loaddataintable();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("All fields Required");
            }
        }

        public void ClearTextBox()
        {
            detail.Clear();
            CidList.SelectedItem = null;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; 
                detail.Text = selectedRow.Cells["Details"].Value.ToString();
                CidList.Text = selectedRow.Cells["CloId"].Value.ToString();
                rid.Text = selectedRow.Cells["Id"].Value.ToString();

            }
        }

        private void Del_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Delete From Rubric where Details=@Details  ", con);
                cmd.Parameters.AddWithValue("@Details", detail.Text);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Deleted");
                loaddataintable();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Any Rubric that you want to Deleted");
            }
        }

        private void update_Click(object sender, EventArgs e)
        {
            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Rubric set Details=@Details where Id=@Id  ", con);
                cmd.Parameters.AddWithValue("@Details", detail.Text); 
                cmd.Parameters.AddWithValue("@CloId", CidList.Text);
                cmd.Parameters.AddWithValue("@Id", rid.Text);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Updated");
                loaddataintable();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Any Rubric that you want to Updated");
            }

        }

        private void Rubric_Level_Button_Click(object sender, EventArgs e)
        {
            this.Hide();
            var newform = new RubricLevel();
            newform.Show();
        }
    }

}
