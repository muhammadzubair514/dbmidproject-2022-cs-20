﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace DBMidProject
{
    public partial class CLO : Form
    {
        public CLO()
        {
            InitializeComponent();
            loadDatainGrid();
        }


        private void label1_Click(object sender, EventArgs e)
        {
            StudentFormOpen();
        }


        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StudentFormOpen();
        }
        //This function is used to open the Student Manager Form
        private void StudentFormOpen()
        {
            this.Hide();
            var newform = new Student();
            newform.Show();
        }

        private void pictureBox5_Click_1(object sender, EventArgs e)
        {
            RubricsFormOpen();
        }


        private void label5_Click(object sender, EventArgs e)
        {
            RubricsFormOpen();
        }
        //This function is used to Rubrics management Form 
        private void RubricsFormOpen()
        {
            this.Hide();
            var newform = new Rubric();
            newform.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            AssessmentFormOpen();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            AssessmentFormOpen();
        }
        //This function is used to Assessment management Form 
        private void AssessmentFormOpen()
        {
            this.Hide();
            var newform = new Assessments();
            newform.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }
        //This function is used to Evaluation management Form 
        private void EvaluationFormOpen()
        {
            this.Hide();
            var newform = new Evaluation();
            newform.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }

        private void label8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }
        //This function is used to Report management Form 
        private void ReportFormOpen()
        {
            this.Hide();
            var newform = new Report();
            newform.Show();
        }


        // This is the Close Label or Picture Event
        private void pictureBox9_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }

        private void label13_Click(object sender, EventArgs e)
        {
            AttendenceFormOpen();
        }
        //This function is used to Attendence management Form 
        private void AttendenceFormOpen()
        {
            this.Hide();
            var newform = new Attendence();
            newform.Show();
        }

        private void loadDatainGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;

        }

        public bool isvalid()
        {
            if (cloname.Text == string.Empty || dateTimePicker1.Value.ToString() == string.Empty || dateTimePicker1.Value.ToString() == string.Empty)
                return false;
            return true;
        }

        private void ADD_Click(object sender, EventArgs e)
        {

            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into Clo values (@Name, @DateCreated, @DateUpdated)", con);
                cmd.Parameters.AddWithValue("@Name", cloname.Text);
                cmd.Parameters.AddWithValue("@DateCreated", dateTimePicker1.Value.ToString());
                cmd.Parameters.AddWithValue("@DateUpdated", dateTimePicker1.Value.ToString());


                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Added New CLO");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("CLO Name Required");
            }
        }

        public void ClearTextBox()
        {
            cloname.Clear();
            dateTimePicker1.Refresh();
            dateTimePicker2.Refresh();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0 && e.RowIndex < dataGridView1.Rows.Count - 1)
            {

                DataGridViewRow selectedRow = dataGridView1.Rows[e.RowIndex]; ;
                cloname.Text = selectedRow.Cells["Name"].Value.ToString();
                cid.Text = selectedRow.Cells["ID"].Value.ToString();
                dateTimePicker1.Text = selectedRow.Cells["DateCreated"].Value.ToString();
                dateTimePicker2.Text = selectedRow.Cells["DateUpdated"].Value.ToString();
            }
        }



        private void update_Click(object sender, EventArgs e)
        {

            if (isvalid())
            {
                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Update Clo set Name=@Name,DateUpdated=@DateUpdated where Id=@Id  ", con);
                cmd.Parameters.AddWithValue("@Name", cloname.Text);
                cmd.Parameters.AddWithValue("@DateUpdated", dateTimePicker2.Value.ToString());
                cmd.Parameters.AddWithValue("@Id",cid.Text);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Successfully Updated");
                loadDatainGrid();
                ClearTextBox();
            }
            else
            {
                MessageBox.Show("Select Any CLO that you want to Updated");
            }
        }
    }
    }

