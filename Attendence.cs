﻿using CRUD_Operations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics.Contracts;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ListView;

namespace DBMidProject
{
    public partial class Attendence : Form
    {
        public Attendence()
        {
            InitializeComponent();
            loadDataINGrid();
            
            
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            StudentWindowOpen();
        }
        //This function is used to Student management Form 
        private void StudentWindowOpen()
        {
            this.Hide();
            var newform = new Student();
            newform.Show();

        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }

        private void label4_Click(object sender, EventArgs e)
        {
            CLOFormOpen();
        }
        //This function is used to CLO management Form 
        //hjhkjk
        //kjjh

        private void CLOFormOpen()
        {
            this.Hide();
            var newform = new CLO();
            newform.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            RubricFormOpen();
        }
        //This function is used to Rubric management Form 
        private void RubricFormOpen()
        {
            this.Hide();
            var newform = new Rubric();
            newform.Show();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            AssessFormOpen();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            AssessFormOpen();
        }
        //This function is used to Assessment management Form 
        private void AssessFormOpen()
        {
            this.Hide();
            var newform = new Assessments();
            newform.Show();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }

        private void label7_Click(object sender, EventArgs e)
        {
            EvaluationFormOpen();
        }
        //This function is used to Evaluation management Form 
        private void EvaluationFormOpen()
        {
            this.Hide();
            var newform = new Evaluation();
            newform.Show();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }

        //This function is used to Report management Form 
        private void ReportFormOpen()
        {
            this.Hide();
            var newform = new Report();
            newform.Show();
        }
        // This is the Close Label or Picture Event
        private void label8_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        public void loadDataINGrid()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select RegistrationNumber,Concat(FirstName,' ',LastName) As StudentName from Student where Status = 5  ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Reload_Click(object sender, EventArgs e)
        {
            loadDataINGrid();
        }

        private void Save_Button_Click(object sender, EventArgs e)
        {
            addattendenceintodatabase();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void ID_Click(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void adddateintodatabase()
        {
            

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into ClassAttendance values (@AttendanceDate)", con);
            cmd.Parameters.AddWithValue("@AttendanceDate", dateTimePicker1.Value.ToString());

            cmd.ExecuteNonQuery();
        }
        //  cmd.Parameters.AddWithValue("@RegistrationNumber", selectedRow.Cells[1].Value.ToString());
        private void addattendenceintodatabase()
        {

            int s = 1;
            adddateintodatabase();

            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                DataGridViewRow selectedRow = dataGridView1.Rows[i];

                var con = Configuration.getInstance().getConnection();
                SqlCommand cmd = new SqlCommand("Insert into StudentAttendance (AttendanceId,StudentId,AttendanceStatus) values ((Select Id from ClassAttendance Where AttendanceDate=@AttendanceDate),(Select Id from Student where RegistrationNumber=@RegistrationNumber AND Status=5) ,@AttendanceStatus)", con);   
              
               object registrationNumberValue = selectedRow.Cells[1].Value;
               string registrationNumber = registrationNumberValue != null ? registrationNumberValue.ToString() : "";
               cmd.Parameters.AddWithValue("@RegistrationNumber", registrationNumber);

                if (selectedRow.Cells["status"].Value != null)
                {
                    string status = selectedRow.Cells["Status"].Value.ToString();

                    if (status == "Present")
                        s = 1;
                    else if (status == "Absent")
                        s = 2;
                    else if (status == "Leave")
                        s = 3;
                    else if (status == "Late")
                        s = 4;
                    else
                        s = 2;
                }
                else
                {
                    s = 2;
                }


                cmd.Parameters.AddWithValue("@AttendanceStatus",s);
                cmd.Parameters.AddWithValue("@AttendanceDate", dateTimePicker1.Value.ToString());
                cmd.ExecuteNonQuery();

            }
           
            


        }


        

        private void report_click(object sender, EventArgs e)
        {
            ReportFormOpen();
        }

        private void dataGridView1_CellFormatting_1(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView1.Columns[e.ColumnIndex] is DataGridViewComboBoxColumn && e.RowIndex >= 0)
            {
                int targetColumnIndex = 0;

                if (e.ColumnIndex == targetColumnIndex)
                {
                    DataGridViewComboBoxCell comboBoxCell = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex] as DataGridViewComboBoxCell;

                    if (comboBoxCell.Value == null)
                    {
                        comboBoxCell.Value = comboBoxCell.Items[0];
                    }
                }
            }

        }
    }
}
